# OpenML dataset: Bitcoin-Stock-Data

https://www.openml.org/d/43779

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This Dataset contains the value of the Bitcoin stock from 14th September 2014 till Date 
Content
It is a very simple dataset to both explore and understand the columns are themselves descriptive in nature
Acknowledgements
SOURCE:
https://yahoofinance.com/ 
Inspiration
Just Explore the dataset, practice your skills using this really simple real world dataset
License
CC0: Public domain

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43779) of an [OpenML dataset](https://www.openml.org/d/43779). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43779/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43779/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43779/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

